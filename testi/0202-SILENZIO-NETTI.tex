%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
%!TEX root = ../metp.tex

\section{Giorgio NETTI: \emph{Il tema del silenzio}}

\thispagestyle{empty}

\begin{multicols}{2}
\begin{singlespace*}
\small

\begin{flushright}
\textit{E’ ancora buio.\\
Siedo al doppio vetro chiuso della cappuccina;\\
notte, silenzio sigillato, barricato disagio.\\
Apro solo un poco la finestra e subito è spazio\\
esteso, animato, orientamento.\\
L’ascolto guida dove lo sguardo non aiuta.\\
Arriva il giorno, poi la luce:\\
siamo già lì,\\
qui.}
\end{flushright}

\noindent È un tema dalla tonalità instabile che imprevedibilmente modula: il
vuoto, il neutro, l’equilibrio, sono alcune fra le sue trasformazioni più
frequenti. Il tema è lo stesso, i luoghi gli strumenti e le interpretazioni
attraverso cui lo si ascolta molto differenti.

L’antico Egitto ne aveva fatto un dio, Arpocrate, un dio bambino che porta alla
bocca il dito indice. Il dio della parola celata, del segreto misterico, era
rasato, con una nutrita treccia di capelli sulla destra; nato debole di gambe
lo rappresentavano seduto su quelle della madre, Isis, o su un loto. Tutte le
successive natività gli hanno fatto eco per secoli.

In epoca greca, sebbene come dio avesse perso il posto, i Misteri rimasero tali
proprio grazie a quel silenzio. Un silenzio che cominciò a figliare, adattandosi
alla progressiva frammentazione del mistero: non più uno, molti e poi moltissimi
misteri, tutti da proteggere. Spuntò un fiore fra le dita, venne fondata
l’eccezionalità di alcuni luoghi, i santi sguardi si fissarono sui muri e sulle
tele, poi anche i meno santi, i corpi, i fatti, i paesaggi, sino a custodire i
colori stessi, i gesti le parole i suoni, ogni volta vissuti e ricomposti in
nuove soluzioni.

E’ un passaggio importante: se il mistero ha infinite facce, non può essere il
contenuto esclusivo di un concetto un oggetto o un’azione uguale per tutti
sempre. Ogni mistero è un buco, un buco nella trama del conosciuto, e come tale
va circoscritto, per proteggersi dai pericoli di una eccessiva sfilacciatura del
conosciuto, e integrato, affinché una nuova trama lo comprenda. Pericolo e
salvezza. Dal silenzio imposto siamo passati al silenzio esposto, e l’uomo
stesso è diventato un operatore del mistero.

Quale buco, quale mistero, quale silenzio? La maggior parte di noi
occasionalmente ancora se lo chiede, si distrae e volta pagina. Alcuni rimangono
in prossimità di un buco esplorandolo in tutte le sue angolature, certi che sia
una fonte infinita di scoperte. Altri raccolgono e organizzano i dati relativi
ai buchi simili, per tentare di capirli e ricostruire una trama. Altri ancora,
intuendo qualcosa dentro e dietro tutti i buchi, ci si immergono. Le reazioni di
fronte allo sconosciuto sono molto differenti, la passione che anima chi
preferisce non distrarsi è simile.

Ogni buco nel conosciuto ha un suo campo gravitazionale più o meno intenso, che
modifica la trama nella quale si è prodotto. Ogni parola, ogni suono segno gesto
azione, è guidata da quei campi interagenti e a suo modo, pesando sulla trama,
la precisa. Il mistero e il suo silenzio, dunque, sono forze sempre attive nel
quotidiano di chiunque, che lo si sappia oppure no.

Oggi si interpreta il silenzio come assenza di suono, il suono della parola in
particolare. La parola che abitualmente usiamo per esprimere dei significati.
Da qui deriva la diffusa equazione, assenza di parola = assenza di significato.

Arte scienza e spiritualità continuano a dimostrare il contrario: là dove la
parola abituale non arriva c’è dell’altro, dell’altro talmente importante da
dedicargli la vita.

Sarebbe meglio dire, dunque, che i significati più consueti usano la parola,
la nostra parola, per rimanere attivi, nei luoghi comuni, nei discorsi
riportati, nella chiacchiera... Rallentando la tessitura di questo infinito
ricamo ci accorgiamo, dicevo, dell’esistenza di una trama a cui il ricamo si
aggrappa. Una trama complessa, stratificata, composta di fili e filtri
organizzati in reti normalmente impercepibili: l’educazione ricevuta, il
contesto di appartenenza, gli studi fatti, il lavoro, gli interessi, i desideri
e molte altre, che costantemente lasciano passare, o catturano, qualcosa e non
qualcos’altro. Trame che caratterizzano il nostro personale stare al mondo.
Implicata, quindi, non è solo la parola ma tutti i sensi.

La molteplicità e l’interconnessione delle reti agenti è ciò che ci fa esseri
unici e in questa unicità si fonda l’inestimabile valore di ogni vita. Ma il
suo valore non è tanto nell’io che la materializza, quanto nella possibilità di
comprendere, attraverso ciò che si deposita nei filtri individuali, qualcosa in
più sulle proprietà del filtrato. In altri termini: filtrando la luce a mio
modo, se sono consapevole dei filtri interposti, posso comprendere qualcosa di
più sulle proprietà della luce stessa, qualcosa che sarà utile anche a chi non
usa gli stessi filtri.

Questo qualcosa di utile lo possiamo chiamare provvisoria invarianza, e dire,
che tutta la trama del conosciuto è fatta di provvisorie invarianze. Sottolineo
provvisorie.

Il deposito delle esperienze inconsapevoli, invece, rende i filtri sempre più
sporchi e inefficaci, formando concrezioni e “calcoli” isolati, nei quali poi
ci identifichiamo dicendo, io sono così.

La limitazione non è nell’io sono ma in quel irrigidito così, che come una
corazza ci preclude ogni ulteriore esperienza.

Allentandone la trama, il silenzio diluisce quel così. Un silenzio che non è
menomazione, quindi, ma una momentanea sospensione del conosciuto. Pratyahara,
un silenzio esteso a tutti i sensi, ben più prezioso di una passeggiata sui
carboni ardenti. Non un’assenza, al contrario, un’intensa presenza
momentaneamente scollegata dalle spinte abituali: un equilibrio.

Cosa distingue questa sospensione attiva dal generico far nulla o, come spesso
si vede, dall’atteggiarsi all’essere depositari di chissà quali impercettibili
verità? Ciò che produce, nella propria vita e in quella altrui.

La pagina genericamente bianca non basta, se è il frutto di una esperienza vera
sarà bianca in una maniera talmente precisa da diventare unica. Come una chiave.

Tacere dunque, o piuttosto, tacersi? ... nell’idea di Dio ancora un grande io
sfarfalla.

I poeti maledetti, gli scienziati scapigliati e gli yogi cosparsi di cenere,
passato il periodo storico che si è espresso con quelle modalità, sono a loro
volta convenzioni e automatismi. Non dobbiamo considerare una necessità locale
come modello per le successive. L’artista romantico è diventato il modello
dell’artista, Einstein quello dello scienziato, l’irreprensibile asceta quello
del meditante: bisogna andare altrove.

Cominciamo con l’evitare di immaginare come dovrebbero essere, cominciamo con
l’evitare di immaginare, cominciamo con l’evitare (la carica dell’ovvio),
cominciamo.

In prossimità del buco tutto rallenta e si concentra. Azzerata un parte di
certezze il sistema si riaggiorna. Se la motivazione è buona, il costo
dell’aggiornamento diventa irrilevante.

L’artista lo scienziato e il meditante maturi hanno la capacità di ricominciare
ogni volta da capo, consapevoli ma non condizionati dalle esperienze precedenti.

L’intuizione abita in questo cominciamento continuo: è un’istante verticale che
annulla l’abituale consequenzialità spazio temporale e pescando ovunque connette
dati, altrimenti dispersi, in una nuova unità.

Rallentiamo ulteriormente, come fa la luce passando attraverso una lente focale
per mostrarci i colori dello spettro. Goccia a goccia, distilliamo.

Il silenzio. Nell’arte converge sul come, nella scienza sul cosa, nello
spirituale sul perché.

Tutto le sue gradazioni agiscono assieme, in percentuali inferiori e instabili
rispetto a quella che caratterizza il filtro dell’operatore agente.

Ogni silenzio ci offre il pericolo di un’ulteriore libertà.

Ogni pagina bianca è un’occasione. Meglio non sprecarla.
\end{singlespace*}
\end{multicols}
\normalsize
