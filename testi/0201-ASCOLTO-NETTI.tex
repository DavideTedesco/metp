%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
%!TEX root = ../metp.tex

\section{Giorgio NETTI: \emph{Verso un ascolto incondizionato}}

\thispagestyle{empty}

\begin{multicols}{2}
\begin{singlespace*}
\small

\begin{flushright}
\textit{E’ ancora buio.\\
Siedo al doppio vetro chiuso della cappuccina;\\
notte, silenzio sigillato, barricato disagio.\\
Apro solo un poco la finestra e subito è spazio\\
esteso, animato, orientamento.\\
L’ascolto guida dove lo sguardo non aiuta.\\
Arriva il giorno, poi la luce:\\
siamo già lì,\\
qui.}
\end{flushright}

\noindent Il suono mi ha sempre più che attratto chiamato, chiamato a qualcosa che ancora
precisamente non so. Dalla mia prima musica scritta sono passati 25 anni, quasi
come quelli trascorsi dalla prima lezione ad un corso di yoga. Ne tanti ne pochi
ma insieme, due metà di un solo intero. Non voglio parlare del legame (lo yoga
del suono, il suono per lo yoga) ne tanto meno della sovrapposizione fra i due
(fare yoga con la musica), ciò che mi sembra più importante approfondire in
questa occasione è l’ascolto, quella attenzione condivisa che silenziosamente
li unisce.

***

Per quanto poco chiaro sia ormai cosa s’intende dicendo yoga, ancora meno è cosa
ognuno di noi associa alla parola suono. Se nel primo caso, l’esponenziale
proliferare delle diverse correnti, scuole, singoli insegnati e presunti tali è
evidente a tutti o quasi, nel secondo, la precaria genericità del termine suono
lo è molto meno; dire suono infatti non è dire musica ma neanche rumore. Se per
suono, fedeli alla Treccani 1979, intendiamo “la causa delle sensazioni acustiche,
consistente in vibrazioni dell’aria, in questa eccitate dalle vibrazioni di un
corpo, eventualmente trasmesse all’aria per il tramite di uno o più mezzi
elastici, e a loro volta eccitanti l’orecchio, generalmente per azione diretta
sul timpano”, c’è già di che seriamente preoccuparsi: il suono sembra non voler
essere definito. Troviamo però una serie di termini già incontrati nella pratica
dello yoga che potrebbero diventare porte e passaggi fra i due ambiti: sensazioni,
vibrazioni, corpo sorgente, mezzi elastici, azione diretta.

Facciamo un passo indietro. Dicendo suono presupponiamo, si, una vibrazione, ma
con questa, l’esistenza di un corpo in grado di vibrare, un attivatore della
vibrazione, un tempo in cui la vibrazione accade e uno spazio in cui la vibrazione
si propaga. Per esempio: il suono di una campana ci raggiunge quando la vibrazione
prodotta dal suo corpo percosso (dal batacchio, l’attivatore), dopo (tempo) aver
attraversato la distanza (spazio) che ci separa dalla campana, arriva a far
vibrare i nostri timpani; e non solo loro, perché a seconda delle caratteristiche
del corpo vibrante, di ciò che lo mette in vibrazione, dell’intensità con cui lo
fa e della distanza alla quale è posta la fonte sonora, a chi ascolta potrebbe
anche vibrare l’intero corpo, ossa comprese. Non bisogna essere degli sciamani
per sperimentarlo, basta entrare qualche secondo in una discoteca o affacciarsi
un istante sul gran premio. La vibrazione acustica, infatti, ci raggiunge e
coinvolge fisicamente anche ben al di là dell’udito, ma non necessariamente in
maniera traumatica: l’estremo opposto all’assalto precedente potrebbe essere
quello stato di massima ricettività chiamato “pelle d’oca” che certe musiche in
certi momenti sono capaci di attivare.

Diverso è il suono di un clacson, di una sirena o di un violino ma in tutti
agiscono gli stessi quattro elementi formanti (l’attivatore, il corpo vibrante
il tempo e lo spazio) che, di volta in volta differentemente interpretati dagli
eventi, modificano la qualità del loro trasmettere e del nostro ricevere la
vibrazione. Potremmo dunque ridefinire il suono come, la manifestazione acustica
generata da un complesso di relazioni fisiche che temporaneamente abitano uno
spazio dato. Definizione più tonda, risonanza più estesa. Anche le definizioni
sono degli attivatori: servono a far vibrare l’intero corpo del senso,
possibilmente da dentro, come i rintocchi per una campana. Vedremo quanto
complessa possa essere la relazione fra attivatore, corpo vibrante e vibrazione
prodotta ma è interessante notare da subito che, in quel abitare temporaneamente
lo spazio, il suono ha un suo inizio, un durare e una fine. Non è di un oggetto
quindi che parliamo ma di un ente che nel tempo si trasforma. La differenza è
importante perché molto spesso, invece, identifichiamo un suono con ciò che lo
produce riducendone enormemente le potenzialità: un suono di campana diventa la
campana, un suono di clacson diventa il clacson, vale a dire, tutti uguali o
quasi. Se il primo ci comunica le ore e il secondo un probabile pericolo
l’identificazione suono-oggetto è sicuramente utile, ma se fuori pericolo gli
prestiamo attenzione ci accorgiamo che il suono, ogni suono, vive,
differentemente dagli altri e mai isolato, preziosamente unico fra i simili;
fuori e dentro a chi ascolta.

La parola suono è breve e risonante in molte lingue, insieme alla vocale c’è
sempre una semiconsonante, suono, son, sound, klang, come a ricordare un esterno
(vocale) ed un interno (semiconsonante), uno spazio aperto e uno quasi chiuso,
qualcosa di comune e qualcosa di personale, che continua ad accadere anche al di
là dell’apparente silenzio. Dove finisce infatti un suono e, in particolare, dove
finisce un suono risonante?

Torniamo al suono di campana ma, oltre alla memoria di quella lassù in cima al
campanile, prendiamo un dobaci, la campana giapponese a forma di tazza che molti
fra noi hanno suonato almeno una volta (la pratica aiuta sempre). In entrambe il
suono nasce nell’istante esatto in cui il battente tocca e, per quanto leggero
sia il colpo iniziale, vibrerà (quasi) subito al massimo dell’intensità,
diminuendo poi fino a scomparire senza che si possa dire esattamente quando.
Proviamo in coppia. La presenza dell’altro apre lo spazio percettivo personale
perché si ascolta e si vede anche attraverso i suoi sensi: uno dei due dirà
“finito” per poi tacere affinché l’altro verifichi.

Nella quasi totalità dei casi il tempo abituale d’attenzione si allunga
notevolmente e, anche al di là del “finito”, ci saranno ancora parecchi secondi
percepibili da entrambi. Questo la prima volta, poi velocemente ci si affina.
E’ la prova di una sordità inconsapevole determinata dall’aspetto funzionale che
si è soliti attribuire al suono: dal momento in cui riconosco cos’è, lo
identifico in un oggetto e interrompo l’ascolto sovrapponendomi con altri
pensieri, parole, azioni che di fatto azzerano il precedente contatto vibratorio.
Ascoltare però non è dare dei nomi, perché per bene che vada potremo nominare
solo ciò che già conosciamo; ascoltare è rimanere fiduciosamente disponibili,
condividere, e solo in seguito, eventualmente, capire. E’ così che ogni vero
ascolto scopre qualcosa, perché diventando lo spazio risonante di ciò che al suo
interno accade, fisicamente collega e riconduce l’apparente estraneità al
contesto che la comprende. Provate ad ascoltare come ascolta la stanza in cui
vi trovate. Provate ad essere la stanza che con-tiene il suono di campana.
Ascoltare ed essere, in questo caso, tendono inaspettatamente a coincidere.

Entriamo più nello specifico, la campana sta ancora vibrando; avendo appena
descritto cosa accade, la vostra prima prova sarà già la seconda ma la durata
della risonanza vi sorprenderà comunque. Alcune brevi considerazioni: avrete
sperimentato quanto il tipo di battente usato e il punto percosso modifichino
il suono; quanto impossibile sia ridare esattamente lo stesso colpo per ottenere
un suono identico al precedente; come sul finire della vibrazione affiorino
altri suoni già presenti nello spazio prima non notati; come non solo attorno
ma anche dentro al suono principale ci siano suoni secondari che diventano
percepibili soprattutto dalla metà della sua durata in poi, e altro ancora.
Ripetendo poi l’esperimento più volte ad intervalli di tempo sempre più ampi e
con una campana diversa, nel confronto e nella sovrapposizione fra le due, la
percezione si affina ulteriormente attivando una sensibilità insospettata. A
questo proposito è interessante ricordare che la profondità di campo è data
proprio dall’avere due orecchie, due occhi, diversi sensi: sentire “l’altra
campana” e ritrovare la preziosa profondità della vita è, anche questa infatti,
una questione d’ascolto.

Non basta; nell’ascolto, è come se l’aria al suono si trasformasse in acqua e
noi in pesci. Provate, allontanandovi a turno dalla campana: fra l’emissione e
la ricezione, lo spazio nel mezzo improvvisamente si addensa, diventando un
fluido invisibile che congiungendoci fisicamente alla fonte sonora trasforma lo
spazio che separa in spazio che unisce. L’antica magia del suono è tutta qui:
le culture che affidano al suono la creazione del mondo enfatizzano proprio
questa sua capacità di materializzare l’invisibile, estraendo dal nulla una
forma, un cosmo dal caos. Negli infiniti gradi di densità della materia, le
vibrazioni acustiche stanno fra la compatta opacità dei corpi e la trasparenza
della luce. Dense quanto basta per poter essere anche dal tatto sentite ma
sufficientemente sottili da passare i muri, ci aprono un vero e proprio mondo
parallelo in cui buona parte dei riferimenti abituali non valgono più: da qui
la diffidenza e quindi la difficoltà di un vero ascolto.

Proseguiamo nello specifico. La campana però è uno strumento tutto sommato
semplice, che dire allora di un flauto, un violino o un pianoforte, per i quali
è possibile emettere una quantità quasi infinita di suoni? La differenza fra
semplice e complesso in questo caso sta nell’articolazione, minima o capillarmente
sviluppata, di cui lo strumento dispone per modificare sensibilmente il suono
che lo caratterizza. Semplificando: tre colpi sulla stesso campana non saranno
identici ma sono simili, tre dita in sequenza sulla stessa corda produrranno
invece tre suoni diversi. Approfondendo: in realtà ci sono almeno due aspetti
della complessità, uno interno, il timbro, e l’altro esterno, l’articolazione.
Il suono di campana è timbricamente più caratterizzato di quello del violino, e
quei tre suoi suoni simili ci introducono al mondo della differenza nella
continuità, un mondo interiore, complesso e semplice assieme, legato al sacro
in molte culture. Dicendo violino, invece, ecco spuntare la zampetta, non del
diavolo ma della musica, perché parlando di articolazione chiamiamo in causa il
movimento, con la relativa suddivisione di tempo e spazio in periodicità
differenti. Il cuore e le membra, l’infinito del di dentro e l’infinito del di
fuori.

Ma a quale suono in particolare ci riferiamo dicendo il suono del violino?
Certo, ci riferiamo alla media di ciò che abbiamo ascoltato provenire da quello
strumento; l’ascolto reale, però, è distante da una media a posteriori quanto
un fatto da un ricordo. Ancora un tocco di campana per verificare: il timbro di
uno strumento è principalmente determinato dalle caratteristiche costruttive del
suo corpo, dalla modalità di messa in vibrazione (l’attivatore che percuote,
sfrega o soffia), dall’intensità e distribuzione dei parziali sulla fondamentale
e, infine, dalla modalità di scomparsa del suono. Sono in ordine d’importanza,
vale a dire che a occhi chiusi, senza sentirne l’inizio e in assenza di
articolazione, la maggior parte dei suoni diventerebbero presenze senza identità,
“animulae vagulae” direbbe l’imperatore Adriano. Alle stesse condizioni, invece,
quello di campana rimarrebbe più riconoscibile. Interessante compensazione: il
suono di campana che esternamente non è quasi articolabile, nel suo timbrato
risuonare ci guida fin dentro al senza identità, affidabile Presenza fra
presenze e faro; non per niente il sacro l’ha adottato.

Dunque dicendo il suono del violino, non ci riferiamo a nessun suono in
particolare ma a quello strumento in quanto contesto e luogo (per non dire mondo),
con delle sue connotazioni timbrico articolatorie specifiche. Possiamo ascoltare
l’intera storia del violino come una continua ridefinizione dei suoi confini
articolatori e timbrici. Ogni strumento infatti ha un centro, la qualità di
suono progressivamente codificata dalla tradizione che lo caratterizza, e una
periferia, vale a dire uno spazio in cui il suo suono si confonde con quello di
altri strumenti, voci, eventi naturali, animali o cose. Da un punto di vista
acustico la proporzione fra i due è uguale a quella di ogni grande città:
piccolo e compatto il centro, enorme e frammentata la periferia. Ma è anche vero
che il centro lentamente si sposta, lo abbiamo visto nei secoli per l’uomo, la
terra, il sole, la galassia, e la periferia cresce a dismisura, dilatando il
centro fino a generare una moltitudine di altri centri, con le rispettive
periferie intrecciate fra loro in una trama irregolare di pieni e vuoti senza
fine. Nel ‘900 in particolare, abbiamo partecipato ad un impressionante spostamento
di confine che, in tutti i campi, ha progressivamente tentato di riempire le
lacune dello scibile, articolando specializzazioni e connessioni di ogni tipo,
in un’indubbia crescita della conoscenza di ciò che abita l’universo. Anche il
vuoto però è cresciuto, apparentemente a dispetto di tanto progresso e, con lui,
il disagio di un occidente ormai esteso quasi al globo intero. Più si articola
la materia e più questa si divide, più cerchiamo di afferrarla e più lei sfugge
la presa: si aprono finestre sempre più ampie, diminuiscono le pareti e si
assottigliano, il sogno di una tana protettrice, una sicurezza, un argomento,
un appiglio, progressivamente evapora e la vibrazione, ancora, ci attraversa.

Ribaltiamo la prospettiva, lasciamo la presa. Nell’ascolto non ci sono pieni e
vuoti ma condizioni differenti (di corpi, tempi e spazi) che modificano le
modalità di esistenza nel continuo che tutto comunque comprende. C’è un
addensarsi e un rarefarsi della materia ma non ci sono gradini, ne salti, ne
livelli effettivi; c’è una continuità della discontinuità, un moto ondoso
generalizzato e localmente imprevedibile. Per l’ascolto, l’intera storia della
musica o di ogni singolo strumento, dell’umanità o di ogni uomo, è un incessante
espandersi e tornare, in avanti e in dietro, nel piccolo e nel grande, in
lunghezza e in profondità, articolando sempre più capillarmente la conoscenza
della natura da tutti condivisa per scoprire, rientrando, il proprio centro
personale spostato; a fasi alterne, distruttive e costruttive, separate e
sovrapposte. In ascolto si assiste ad una trasformazione generalizzata che
avviene per infiniti passaggi, con tutti i suoi rischi, cadute, crolli, slanci e
riprese; un fondersi complesso di esterno e interno, articolazione e timbro,
postura e attitudine.

Rientriamo: prima di suonare il musicista accorda lo strumento, vale a dire
precisa il contesto all’interno del quale sarà possibile ascoltare anche le
minime differenze di altezza. Dunque dal singolo suono alla musica, prima ancora
che una questione d’orecchio è una questione di contesto: stabilito il contesto,
il senso di una musica sarà l’arte di rendere acusticamente percepibili alcune
delle relazioni interne a quel contesto, relazioni tanto più necessariamente
profonde quando il contesto è ampio.

Proviamo così a tentare un accordarsi più antico e radicale, precedente alle
altezze e agli strumenti; un accordarsi nel metallo, nel legno, nella pelle,
ma non solo, un accordarsi ai soffi, agli attriti, agli impulsi; un accordars
che collega il minimo fruscio ai venti d’altopiano, la carezza dell’aria alla
frenata improvvisa, il cuore di un bambino ai fuochi d’artificio, senza saltare
subito al chi e a cosa produce, rimanendo nel come, nella ricchezza acustica d
ciò che sta accadendo esattamente adesso. Passiamo dall’accordare uno strumento
all’usare l’ascolto stesso come strumento e quindi proviamo ad accordare
l’ascolto: poi senza dare nomi a ciò che si sente, ascoltare-essere una stanza
dicevamo, si, ma anche una sala, una chiesa, una cattedrale; una via, un viale,
una piazza; un accento, un dialetto, una lingua che non si conosce; il proprio
respiro, quello di chi ti siede accanto, di chi vive lontano o non è più.

Dall’interno ascoltare l’esterno, oltre i muri, oltre i vetri, oltre il rumore
dei nostri pensieri; dall’esterno ascoltare l’interno e in-oltre partecipare.
E’ così che il silenzio, superata la chimerica assenza di rumore, di suono, di
pensiero, diventa il massimo contesto: una Presenza diffusa, personale e
impersonale, infinitamente ricettiva e indisturbabile, per la quale ogni
manifestazione, dalla più densa alla più rarefatta, contribuisce ad ampliare
l’ambito del Senso, in quanto “sentita testimonianza” di un ascolto incondizionato.
Da questa estesa accordatura si può rinascere in ogni istante, a nuova musica,
a nuova pratica, a nuova vita.
\end{singlespace*}
\end{multicols}
\normalsize
